# sw3d-threejs

## Name
Sweet Home 3D to three.js Demo

## Description
This project is a Demo to load a model made on Sweet Home 3D sofware https://www.sweethome3d.com into three.js https://threejs.org and visualize it in a web app.

## Installation
To install the dependencies of the project:

npm install

## Usage
To start the development enviroment:

npx vite


To build a static site:

npx vite build

## Authors and acknowledgment
You can follow me on X https://x.com/AlxCoder