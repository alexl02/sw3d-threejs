import * as THREE from 'three';
import { ColladaLoader } from 'three/examples/jsm/loaders/ColladaLoader';
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader';
import { OrbitControls } from 'three/addons/controls/OrbitControls.js';
import { MTLLoader } from 'three/examples/jsm/loaders/MTLLoader';
import { FlyControls } from 'three/addons/controls/FlyControls.js';

// Crear un elemento <div> para mostrar mensajes
// Create an element <div> to show messages
const msgDiv = document.createElement('div');
msgDiv.style.position = 'absolute';
msgDiv.style.top = '100px';
msgDiv.style.left = '100px';
msgDiv.style.padding = '10px';
msgDiv.style.background = 'rgba(255, 255, 255, 0.8)';
msgDiv.style.border = '2px solid #000';
msgDiv.style.fontSize = '30px';
msgDiv.style.display = 'none';
document.body.appendChild(msgDiv);

function showMessage(msg) {
    // Actualiza el contenido del elemento <div> con el mensaje
    // Refresh the content of the element <div> with the message
    msgDiv.innerHTML = msg;
  
    // Muestra el elemento <div>
    // Shows the element <div>
    msgDiv.style.display = 'block';
  
    // Oculta el mensaje después de un tiempo (puedes ajustar el tiempo según tus necesidades)
    // Hide the message after a while
    setTimeout(() => {
      msgDiv.style.display = 'none';
    }, 3000); // Hide the message after a while
}

//Declare raycaster to get the object which receive the click action.
const raycaster = new THREE.Raycaster();
const mouse = new THREE.Vector2();

//Create the Scene and the Camera.
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );

const renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );
var loader = new OBJLoader();
var mtlloader = new MTLLoader();

//Create the controls that manage the Scene
const controls = new OrbitControls( camera, renderer.domElement );
controls.enableDamping = true;
controls.campingFactor = 0.25;
controls.enableZoom = true;

//Set the Camera position.
camera.position.set( 0,20, 100 );
controls.update();

//const gridHelper = new THREE.GridHelper( 100, 100 );
//scene.add( gridHelper );


//Set the click event
document.addEventListener('mousedown', onDocumentMouseDown);


//Callback function for the click event
function onDocumentMouseDown(event) {
    // Calcula las coordenadas del ratón en el lienzo
    // Calculate the coordinates of the mouse over the canvas
    mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
  
    // Actualiza el raycaster
    // Update the raycaster
    raycaster.setFromCamera(mouse, camera);
  
    // Obtiene una lista de objetos intersectados por el rayo
    // Get a list of the objects intersected by the ray
    const intersects = raycaster.intersectObjects(scene.children, true);
  
    // Verifica si se hizo clic en algún objeto
    // Check if any object was clicked
    if (intersects.length > 0) {
      // El primer objeto en la lista de intersecciones es el más cercano al clic
      // The first object of the list of intersections is the closest to the click
      const selectedObject = intersects[0].object;
      console.log(selectedObject)

      if (selectedObject.name === '1_167') {
        mostrarMensaje('El espacio ' + selectedObject.name + ' esta disponible.');
      }

      if (selectedObject.name === '1_201') {
        showMessage('El espacio ' + selectedObject.name + ' esta disponible.');
      }

      if (selectedObject.name === '22_181') {
        showMessage('El espacio ' + selectedObject.name + ' esta disponible.');
      }

      if (selectedObject.name === '10_173') {
        showMessage('El espacio ' + selectedObject.name + ' esta disponible.');
      }

      if (selectedObject.name === '10_207') {
        showMessage('El espacio ' + selectedObject.name + ' esta disponible.');
      }

      if (selectedObject.name === '1_235') {
        showMessage('El espacio ' + selectedObject.name + ' esta disponible.');
      }

      //alert("Espacio disponible para arrendar.");
  
      // Realiza acciones con el objeto seleccionado, por ejemplo, cambia su color
      //objetoSeleccionado.material.color.set(0xff0000);
    }
}

try {
    // Load the materials file
    mtlloader.load('models/prueba_supermercado.mtl', (materials) => {
        loader.setMaterials(materials);
        // Once the materials file was loaded correctly it load the model file.
        loader.load("models/prueba_supermercado.obj", function (result) {
            console.log(result);

            // Set the scale of the model to be visualized correctly
            result.scale.set(0.02, 0.02, 0.02);

            /************************* CENTRAR MODELO ************************************* */
            /************************* CENTER THE MODEL ************************************* */
            const boundingBox = new THREE.Box3().setFromObject(result);
            const modelSize = boundingBox.getSize(new THREE.Vector3());
            console.log(modelSize);

            result.position.x = (modelSize.x / 2) * -1;
            //result.position.z = (modelSize.z / 2) * -1;
            /****************************************************************************** */
            scene.add(result);
        });
    });
} catch (error) {
    console.log(error);
}

// Set the ligths
var backLight = new THREE.DirectionalLight(0xcccccc, 1.0);
backLight.position.set(0, 1, 0);

scene.background = new THREE.Color(0xffffff); // White background

const light = new THREE.AmbientLight( 0xffffff, 0.3 ); // soft white light

scene.add( light );
scene.add(backLight);


// Render the Scene
function render() {
    requestAnimationFrame(render);
    renderer.render(scene, camera);
}
render();




